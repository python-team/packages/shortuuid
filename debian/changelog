shortuuid (1.0.13-1) unstable; urgency=medium

  * Team upload.
  * Build using pybuild-plugin-pyproject and Poetry.
  * Remove now-unused build-dependency on python3-pep8.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Sun, 26 May 2024 12:44:07 +0100

shortuuid (1.0.11-2) unstable; urgency=medium

  * Allow stderr in autopkgtest

 -- Martin <debacle@debian.org>  Sun, 29 Jan 2023 13:11:41 +0000

shortuuid (1.0.11-1) unstable; urgency=medium

  * New upstream release
  * Add autopkgtest

 -- Martin <debacle@debian.org>  Fri, 27 Jan 2023 20:13:33 +0000

shortuuid (1.0.8-2) unstable; urgency=medium

  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-all.
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 May 2022 19:42:43 +0100

shortuuid (1.0.8-1) unstable; urgency=medium

  * New upstream release; Closes: #1002445
  * Move to Python team; Closes: #1006201
  * debian/control
    - add Python team Vcs fields
    - add Rules-Requires-Root: no
    - remove redundant mention of Python 3
    - new upstream homepage

 -- Martin <debacle@debian.org>  Mon, 21 Feb 2022 10:16:11 +0000

shortuuid (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Build-Depends: Appends debhelper-compat (= 13).
    - Bumps Standards-Versions to 4.5.1.
  * Removes debian/compat.
  * debian/watch
    - Fixes debian-watch-uses-insecure-uri.
    - Bumps version to 4.
  * Removes debian/patches/update_egg-info.patch.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sun, 31 Jan 2021 11:02:22 +0900

shortuuid (0.5.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #938478

 -- Sandro Tosi <morph@debian.org>  Fri, 24 Jan 2020 00:05:18 -0500

shortuuid (0.5.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - Standards-Version: Bumps version to 4.0.0.
  * debian/compat
    - Bumps version to 9.
  * debian/patches
    - Deletes update_egg-info.patch

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sat, 16 Sep 2017 17:22:09 +0900

shortuuid (0.4.3-2) unstable; urgency=medium

  * debian/control
    Standards-Version: Bumps version to 3.9.8.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sat, 02 Jul 2016 06:56:39 +0900

shortuuid (0.4.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS. Add python-pep8 to build-depends. Closes: #818060.

 -- Brian May <bam@debian.org>  Mon, 02 May 2016 11:12:25 +1000

shortuuid (0.4.3-1) unstable; urgency=medium

  * New upstream release

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sat, 13 Feb 2016 15:11:18 +0900

shortuuid (0.4.2-4) unstable; urgency=medium

  * Fixes incompatible with pep8 1.6.2-0.1. (Closes: #799310)
    Thanks report and patch Chris Lamb <lamby@debian.org>.
    Adds debian/patches/Fixed-incompatible-with-pep8-1.6.2-0.1.patch.
  * debian/control
    Adds dh-python to Build-Depends.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Thu, 08 Oct 2015 08:13:44 +0900

shortuuid (0.4.2-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Wed, 01 Jul 2015 13:59:45 +0900

shortuuid (0.4.2-2) experimental; urgency=medium

  * debian/watch
    - Updated url.
  * debian/control
    - Standards-Version: Bumped version.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sun, 19 Apr 2015 07:33:59 +0900

shortuuid (0.4.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Stop relying on matching views about architecture-specific bits
    (DEB_HOST_ARCH_OS and DEB_HOST_GNU_CPU) from dpkg and setuptools
    for the 2.7 build directory; use a wildcard instead. This fixes
    the FTBFS on i386 (Closes: #769262).

 -- Cyril Brulebois <kibi@debian.org>  Fri, 14 Nov 2014 13:32:16 +0000

shortuuid (0.4.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    - Append X-Python3-Version: >= 3.2.
    - Append quilt to Build-Depends.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Thu, 07 Aug 2014 11:03:57 +0900

shortuuid (0.3.2-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    * Append pep8, python3-pep8 to Build-Depends.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Thu, 03 Apr 2014 18:01:46 +0900

shortuuid (0.3.1-2) unstable; urgency=medium

  * debian/control:
    - Update 3.9.5 at Standards-Version.
  * Support python3 version
    - debian/control:
      * Append python3-shortuuid.
      * Append python3-all, python3-setuptools to Build-Depends.
      * Append ">= 3.2" to X-Python-Version.
    - debian/rules:
      * Append override_dh_auto_build, override_dh_auto_install,
        override_dh_auto_clean.
      * Append test for python3 to override_dh_auto_test.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Wed, 19 Feb 2014 09:40:12 +0900

shortuuid (0.3.1-1) unstable; urgency=low

  * Initial release (Closes: #717542)
  * debian/patches
    - update_egg-info.patch: append setup.cfg to SOURCES.txt

 -- Kouhei Maeda <mkouhei@palmtb.net>  Thu, 29 Aug 2013 22:37:51 +0900
