Source: shortuuid
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Kouhei Maeda <mkouhei@palmtb.net>,
           Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
               debhelper (>= 8.0.0),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-poetry-core,
               quilt
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/shortuuid.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/shortuuid
Homepage: https://github.com/skorokithakis/shortuuid/

Package: python3-shortuuid
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: generates concise, unambiguous, URL-safe UUIDs for Python 3
 Often, one needs to use non-sequential IDs in places where users will see them,
 but the IDs must be as concise and easy to use as possible. shortuuid solves
 this problem by generating uuids using Python's built-in uuid module and then
 translating them to base57 using lowercase and uppercase letters and digits,
 and removing similar-looking characters such as l, 1, I, O and 0.
